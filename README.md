# devcont-ruby

A development container (docker / k8s etc..) for ruby, with built in SSH server allowing for convenient use by rubymine but just as useful for command line as well.

## Warning

This is a development container and contains an SSH server with a nice simple password that is in this repository,
meaning that if you expose this SSH server to the internet in your setup, anyone who wants to can log in to the
container.
To make matters worse, it has passwordless sudo, so they can become root user very easily.

It is not intended to be secure, it is intended to be convenient.  If you use it on your private docker instance or
within a kubernetes cluster you will be fine unless you do something to expose the SSH server.

## What It Includes

- An SSH server - so you can connect to it from the command line, from a rubymine IDE (or any other ide that supports 
  SSH)
- A pre configured user 'devuser' with a nice simple password 'password'
- Rsync - rubymine needs this to grab the list of gems installed when used as a remote SDK, can also be used
  generally for syncing things up.
- Passwordless sudo - allows things like rvm to be configured for you - as it does not need to ask for a password
- RVM - With 2.7.3 installed by default (in future, this will be configurable)

##
