# Use phusion/baseimage as base image. To make your builds reproducible, make
# sure you lock down to a specific version, not to `latest`!
# See https://github.com/phusion/baseimage-docker/blob/master/Changelog.md for
# a list of version numbers.
FROM phusion/baseimage:focal-1.0.0

# ...put your own build instructions here...
COPY image/services/ssh/sshd_config /etc/ssh/sshd_config
COPY image/services/my_init/copy_container_environment /etc/my_init.d/00_copy_container_environment
COPY image/services/my_init/chown_rubymine_helpers /etc/my_init.d/00_chown_rubymine_helpers
COPY image/services/syncthing/run /etc/service/syncthing/run
# Syncthing for keeping files in sync with other machines
# Add Syncthing repos
RUN curl -s -o /usr/share/keyrings/syncthing-archive-keyring.gpg https://syncthing.net/release-key.gpg
RUN echo "deb [signed-by=/usr/share/keyrings/syncthing-archive-keyring.gpg] https://apt.syncthing.net/ syncthing stable" | tee /etc/apt/sources.list.d/syncthing.list

# Packages required by any ruby project
RUN curl -fsSL https://deb.nodesource.com/setup_14.x | bash - && apt-get install -y sudo rsync shared-mime-info git libpq-dev nodejs syncthing x11vnc net-tools wget unison
RUN npm install -g yarn
## Create a user for the web app.
RUN addgroup --gid 9999 devuser && \
    adduser --uid 9999 --gid 9999 --disabled-password --gecos "Application" devuser && \
    usermod -L -g sudo devuser && \
    mkdir -p /home/devuser/.ssh && \
    chmod 700 /home/devuser/.ssh && \
    chown devuser:devuser /home/devuser/.ssh
RUN echo "devuser:password" | chpasswd
RUN echo "devuser ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/devuser-user
RUN chmod 0440 /etc/sudoers.d/devuser-user
# Enable SSH
RUN rm -f /etc/service/sshd/down

# Regenerate SSH host keys. baseimage-docker does not contain any, so you
# have to do that yourself. You may also comment out this instruction; the
# init system will auto-generate one during boot.
RUN /etc/my_init.d/00_regen_ssh_host_keys.sh
RUN /usr/sbin/enable_insecure_key

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

USER devuser
COPY image/rubymine-gems /home/devuser/rubymine-gems
RUN gpg --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
RUN curl -sSL https://get.rvm.io | bash -s stable
RUN bash -lc "rvm install 2.7.3"
RUN bash -lc "gem install /home/devuser/rubymine-gems/*"
RUN mkdir /home/devuser/app
CMD ["/usr/bin/sudo", "/sbin/my_init"]
